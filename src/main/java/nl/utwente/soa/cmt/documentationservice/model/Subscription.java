package nl.utwente.soa.cmt.documentationservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Subscription {

    private String url;

    private Long addedAt;

    public Subscription(String url, Long addedAt) {
        this.url = url;
        this.addedAt = addedAt;
    }

    public Subscription(String url) {
        this(url, System.currentTimeMillis() / 1000);
    }

    public Subscription() {}
}
