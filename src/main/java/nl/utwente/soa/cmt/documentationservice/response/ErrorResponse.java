package nl.utwente.soa.cmt.documentationservice.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorResponse {

    private boolean success;

    private String message;

    public ErrorResponse(String message) {
        this.success = false;
        this.message = message;
    }
}
