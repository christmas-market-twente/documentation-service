package nl.utwente.soa.cmt.documentationservice.controller;

import nl.utwente.soa.cmt.documentationservice.dto.response.SwaggerConfigResponseDto;
import nl.utwente.soa.cmt.documentationservice.dto.response.SwaggerUrlResponseDto;
import nl.utwente.soa.cmt.documentationservice.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class DocumentationController {

    @Autowired
    private SubscriptionService subscriptionService;

    @GetMapping("/swagger-config")
    public SwaggerConfigResponseDto swaggerConfig() {
        List<SwaggerUrlResponseDto> urls = subscriptionService.getSubscriptions()
            .entrySet()
            .stream()
            .sorted(Map.Entry.comparingByKey())
            .map(e -> new SwaggerUrlResponseDto(e.getKey(), e.getValue().getUrl()))
            .collect(Collectors.toList());

        return new SwaggerConfigResponseDto(urls);
    }
}
