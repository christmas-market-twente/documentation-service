package nl.utwente.soa.cmt.documentationservice.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SwaggerConfigResponseDto {

    private List<SwaggerUrlResponseDto> urls;

    public SwaggerConfigResponseDto(List<SwaggerUrlResponseDto> urls) {
        this.urls = urls;
    }

    public SwaggerConfigResponseDto() {}
}
