package nl.utwente.soa.cmt.documentationservice.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubscriptionRequestDto {

    private String name;

    private String url;
}
