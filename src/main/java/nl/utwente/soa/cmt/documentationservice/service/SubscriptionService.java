package nl.utwente.soa.cmt.documentationservice.service;

import nl.utwente.soa.cmt.documentationservice.dto.request.SubscriptionRequestDto;
import nl.utwente.soa.cmt.documentationservice.model.Subscription;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class SubscriptionService {

    private final Map<String, Subscription> subscriptions = new ConcurrentHashMap<>();

    public Map<String, Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void subscribe(SubscriptionRequestDto subscriptionRequestDto) {
        subscriptions.put(
            subscriptionRequestDto.getName(),
            new Subscription(subscriptionRequestDto.getUrl())
        );
    }

    @Scheduled(fixedRate = 10 * 60 * 1000)
    public void checkSubscriptions() {
        long now = System.currentTimeMillis();
        // If the subscription is added over 30 minutes ago, then remove it
        subscriptions.values().removeIf(s -> s.getAddedAt() + 30 * 60 > now);
    }
}
