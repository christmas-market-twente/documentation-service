package nl.utwente.soa.cmt.documentationservice.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SwaggerUrlResponseDto {

    private String name;

    private String url;

    public SwaggerUrlResponseDto(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public SwaggerUrlResponseDto() {}
}
