package nl.utwente.soa.cmt.documentationservice.controller;

import nl.utwente.soa.cmt.documentationservice.dto.request.SubscriptionRequestDto;
import nl.utwente.soa.cmt.documentationservice.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SubscriptionController {

    @Autowired
    private SubscriptionService subscriptionService;

    @PostMapping("/subscribe")
    public void subscribe(@RequestBody SubscriptionRequestDto subscriptionRequestDto) {
        subscriptionService.subscribe(subscriptionRequestDto);
    }
}
